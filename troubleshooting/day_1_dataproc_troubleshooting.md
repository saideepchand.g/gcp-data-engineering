1. in **myenv** file, put the MYREGION & ZONE in double quotes like this
   ```bash
    MYREGION="us-central1"
    MYZONE="us-central1-c"
    BUCKET="qwiklabs-gcp-b770345986805626"
    BROWSER_IP="127.0.0.1"
   ```
2. If you can't see output of echo $BUCKET, then execute ```source myenv```