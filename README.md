# First login Instructions

1. Create a googlecloud.qwiklabs.com & Login to googlecloud.qwiklabs.com with your company account
  2. If it says, username is already taken, say forgot password, and retrieve the password and then login. 
1. If you can't find Data Engineering Lab in Classroom Options, enter your email id in form `https://goo.gl/forms/cSR5I8UHgl6Ytsxs1`
1. **Login to qwiklabs using company email id** & navigate to corresponding lab & click start lab. After lab is started **login to google cloud console using tmp username & password created by qwiklabs**
1. Login to google cloud console using tmp username & password, INSIDE INCOGNITO WINDOW
1. Change the project by clicking on select project and choose the project which begins with "qwiklabs-gcp-<something>"


# DataProc Simple Lab
- https://codelabs.developers.google.com/codelabs/cloud-dataproc-starter/#0