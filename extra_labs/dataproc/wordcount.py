from __future__ import print_function
import pyspark
import sys

sc = pyspark.SparkContext()

text_file = sc.textFile("gs://mystical-glass-199311/pg9296.txt")
words = text_file.flatMap(lambda line: line.split())
wordmap = words.map(lambda word: (word, 1))
wordCounts = wordmap.reduceByKey(lambda count1, count2: count1 + count2)
pairs = wordCounts.collect()
for (word,count) in pairs:
    print(word,count)